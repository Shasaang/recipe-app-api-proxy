# recipe-app-api-proxy

NGINX Proxy app for recipe app API

## Usage

### Environment Variable

 * 'LISTEN_PORT' - Port to listen on (default: 8000)
 * 'APP_HOST' - Host name of the app to forward request (default: app)
 * 'APP_PORT' - Port of the app to forward request (default: 9000)