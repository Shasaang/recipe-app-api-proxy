#Base Image
FROM nginxinc/nginx-unprivileged:1-alpine

# labelling maintainer of the image
LABEL maintainer='maintainer@gmail.com'

#Copying the config and parameter files to nginx
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

#Setting environmental variables
ENV LISTEN_PORT=8000
ENV APP_HOST=app 
ENV APP_PORT=9000

#Switching to root user
USER root

#creating a static folder and changing its permission to owner
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static

#creating a new file and changing its owner to nginx
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

#Copying the entrypoint file and making it executable
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

#Switching the user back to nginx
USER nginx

#Executing the command
CMD ["/entrypoint.sh"]
